/* File name:DBConnection.java
 * Project created: 3:40:20 AM Project name:Demo_Login
 * RABI
 * Copyright © 2015 Md.Rabiul Ali Sarker. All Rights Reserved.
 */


import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;

/**
 *
 * @author RABI
 */
public class DBConnection {
    Connection conn = null;
    public static Connection connect(){
        try{
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:myusers.sqlite");
            System.out.println("connect!!!!!");
            return conn;
        }catch(ClassNotFoundException | SQLException | HeadlessException e){
             JOptionPane.showMessageDialog(null, e);
             
        }
        return null;
    }
    
}
